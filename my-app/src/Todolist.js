import React, { Component } from 'react';
import TodoItems from "./TodoItems";

import './custom.css';

class Todolist extends Component {

  constructor(props) {
    super(props);

    this.state = {
      items: []
    };

    this.addItem = this.addItem.bind(this);
  }

  addItem(e) {
    if (this._inputElement.value !== "") {
      var newItem = {
        text: this._inputElement.value,
        key: Date.now()
      };

      this.setState((prevState) => {
        return {
          items: prevState.items.concat(newItem)
        }
      });
    }


    this._inputElement.value = "";

    console.log(this.state.items);


    e.preventDefault();


  }


  render() {
    return (
      <div className="todolnpmist-content">
        <form onSubmit={this.addItem}>
          <input ref={(a) => this._inputElement = a } type="text" placeholder="add item" />
          <button type="submit">submit</button>
        </form>
        <TodoItems entries={this.state.items} />
      </div>

     
    );
  }
}

export default Todolist;
